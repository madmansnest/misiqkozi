import { createApp } from '/vue.js'

function convert(s, addBidiCC) {
  return s.split('\n').map(l => convert_line(l, addBidiCC)).join('<br>')
}

function convert_line(l, addBidiCC) {
  var converted_line = l.split(' ').map(w => convert_söz(w)).join(' ')
  if (addBidiCC == true) {
    converted_line = '\u2067' + converted_line + '\u2069'
  }
  return converted_line
}

function convert_söz(w) {
  const lookup = {
  	'а':'ا', 
  	'ә':'ا', 
  	'б':'ب', 
  	'в':'ۆ', 
  	'г':'گ', 
  	'ғ':'ع', 
  	'д':'د', 
  	'е':'ە', 
  	'ё':'يو', 
  	'ж':'ج', 
  	'з':'ز', 
  	'и':'ي', 
  	'й':'ي', 
  	'к':'ك', 
  	'қ':'ق', 
  	'л':'ل', 
  	'м':'م', 
  	'н':'ن', 
  	'ң':'ڭ', 
  	'о':'و', 
  	'ө':'و', 
  	'п':'پ', 
  	'р':'ر', 
  	'с':'س', 
  	'т':'ت', 
  	'у':'ۋ', 
  	'ұ':'ۇ', 
  	'ү':'ۇ', 
  	'ф':'و', 
  	'х':'ح', 
  	'һ':'ھ', 
  	'ц':'تس', 
  	'ч':'چ', 
  	'ш':'ش', 
  	'щ':'شش', 
  	'ъ':'', 
  	'ы':'ى', 
  	'і':'ى', 
  	'ь':'', 
  	'э':'ە', 
  	'ю':'يۋ', 
  	'я':'يا', 
  	',':'،',
    '?':'؟'
  }
  w = w.toLowerCase()
  
  var out = w.split('').map(function(c){
    if (c in lookup) {
      return lookup[c]
    } else {
      return c
    }
  }).join('')
  if (!/[г|е|к]/g.test(w)) {
    if (/[ә|ө|ү|і]/g.test(w)) {
      out = 'ٔ' + out
    }
  }
  // Special case for forms of the word би
  if (['би', 'бинің', 'биді', 'биі'].includes(w)) {
    out = 'ٔ' + out
  }
  return out
}

createApp({
  data() {
    return {
      wayin: '',
      outfont: 'Naskh',
      bidi: true
    }
  },
  computed: {
    wayout() {
      return convert(this.wayin, this.bidi)
    }
  },
  methods: {
    copyToClipboard: function(event) {
      navigator.clipboard.writeText(this.wayout)
    }
  }
}).mount('#app')
